# CSSLP

Certified Secure Software Lifecycle Profesional is the credential assuring software professionals as capable of implementing security within systems development lifecycle. It is intended for Programmers, Software Architects, Project Managers, Software Auditors and in general those interested in knowing the techinques and methodologies of Secure Software Development.

## (ISC)2

The <span style="color: #cc0">International Information System Security Certification Consortium</span> is the most prestiged <span style="color: #c00">information system security</span> <span style="color: #55f">certification organization</span> with more than 80,000 members in 135 countries.

## About CSSLP

<span style="color: #cc0">Vulnerable software</span> is one of the fundamental causes for many <span style="color: #c00">security incidents</span>, given the <span style="color: #55f">ever more complex nature of software</span>, said vulnerabilities are not a problem that can be solved in the short term.

<span style="color: #0c0">Reducing the amount and graveness of vulnerabilities is possible and useful</span> in software projects. CSSLP is <span style="color: #cc0">not only centered on the aspects regarding <span style="color: #0c0">secure software development</span> <span style="color: #c00">but also those of the network and server</span> where software will be executed</span>.

## CSSLP CBK Domain

1. Secure Software Concepts
1. Secure Software Requirements
1. Secure Software Design
1. Secure Software Coding/Implementation
1. Secure Software Testing
1. Secure Software Acceptation
1. Software Implementation, Operations, Maintenance and Elimination
1. Supply chain acquisition and software

---

1. ## Secure Software Concepts

<details>

1. ### General Security Concepts

Secure software development is strongly linked to the .

<details>
</details>

1. ### Risk Management

<details>

Risk management processes include a <span style="color: #0c0">preliminar evaluation</span> of the need for <span style="color: #cc0">security controls</span>, <span style="color: #0c0">the identification, development, testing, implementation and verification (evaluation)</span> of security controls for the <span style="color: #cc0"><span style="color: #c00">impact of any disruptive process</span> to be in an acceptable or appropiate level for a certain risk</span>.

Before deepening into the challenges of secure software development risk management, it is imperative to have a <span style="color: #0c0">solid understanding of the terms and formulas for <span style="color: #c00">risk calculation</span></span> used in the context of traditional risk management.

1. Risk

   <span style="color: #cc0">Risk</span> is the <span style="color: #cc0">possibility of <span style="color: #c00">suffering damages</span> or losses</span>.

1. Asset

   Assets <span style="color: #cc0">are <span style="color: #0c0">elements valuable</span> for the organization</span>, <span style="color: #cc0"><span style="color: #c00">the loss</span> of which could potentially cause interruptions in the capability of the organization to fulfill its missions</span>.

   Other reasons that impose the need for protecting assets are <span style="color: #0c0">the fulfillment of legal regulations</span>, <span style="color: #cc0">privacy</span> or the need for a <span style="color: #55f">competitive advantage</span>.

   Assets can be <span style="color: #f50">tangible</span> or <span style="color: #55f">intangible</span>. <span style="color: #f50">Data</span> is the most important tangible asset, only outvalued by <span style="color: #f50">people</span>.

   <span style="color: #55f">Intangible assets</span> include <span style="color: #55f">customer loyalty, copyrights, patents, trademarks and brand reputation</span>. The loss or damage of a brand's reputation can be almost impossible to fix.

1. Vulnerability

   A <span style="color: #cc0">weakness or failure</span> that could be <span style="color: #fc0">provoked accidentally</span> or <span style="color: #c00">exploited intentionally</span> by an <span style="color: #c00">attacker</span> (i.e. a <span style="color: #c00">threat</span>) to cause damage, resulting in the <span style="color: #c00">violation</span> or <span style="color: #f50">failure</span> of <span style="color: #0c0">security policy</span>, is known as <span style="color: #cc0">vulnerability</span>.

   <span style="color: #cc0">Vulnerabilities</span> can be <span style="color: #0c0">evident</span> in the process, design or implementation of a system or software.

1. XSS (Cross Site-Scripting)

   These are a type of vulnerability in web sites which <span style="color: #c00">allows attackers</span> to input a <span style="color: #c00">malicious script</span> to be <span style="color: #55f">executed in <span style="color: #0c0">trusted web sites or applications</span> installing <span style="color: #c00">malware</span> in users' web browsers</span>.

1. Threat

   Those <span style="color: #cc0">vulnerabilities</span> representing a <span style="color: #f50"><span style="color: #c00">threat</span> to assets</span>.

   It can be simply, <span style="color: #cc0">the possibility for an undesired, unintentional or <span style="color: #c00">harmful</span> event to happen</span>, resulting in an <span style="color: #cc0">incident</span>.

   Threats can be classified as <span style="color: #cc0">divulgation, alteration or destruction</span>.

1. Source of threat / Agent

   Anything with the potential to create a <span style="color: #c00">threat</span>.

   <span style="color: #c00">Threat agents</span> can be <span style="color: #f50">human</span> or <span style="color: #55f">non-human</span>.

   Non-human <span style="color: #c00">threat agents</span> remaining today can be, <span style="color: #0c0">beside nature itself</span>, <span style="color: #cc0">malware such as adware, spyware, virus and worms</span>.

1. Mitigation

   Mitigation refers to <span style="color: #cc0">any action taken to <span style="color: #0c0">reduce</span> the likelihood of a <span style="color: #c00">threat ocurrence</span></span>.

1. Attack

    <span style="color: #c00">Threat agents</span> can <span style="color: #f50">unintentionally cause threats</span> or can even occur as a <span style="color: #f50">simple <span style="color: #c00">user error</span> or as an accidental discovery</span>.
    
    When the <span style="color: #c00">threat agent</span> activates and <span style="color: #cc0">unintentionally provokes a <span style="color: #c00">threat to be produced</span>, it is denominated <span style="color: #c00">attack</span></span> and threat agents are commonly denominated <span style="color: #c00">attackers</span>.

    When an <span style="color: #c00">attack</span> occurs as the result of an <span style="color: #c00">attacker</span> <span style="color: #cc0">taking advantage of a <span style="color: #f50">known vulnerability</span></span> it is known as an <span style="color: #c00">exploit</span>.

1. Likelihood

    The <span style="color: #cc0">likelihood</span> of a <span style="color: #c00">particular threat</span> occurring. Given that the <span style="color: #0c0">goal</span> of <span style="color: #cc0">risk management</span> is reducing the risk to an acceptable level, <span style="color: #cc0">it is important to measure the <span style="color: #f50">likelihood</span> of an <span style="color: #f50">unintended, undesired or <span style="color: #c00">harmful</span> event</span> happening</span>.

1. Impact

    The result of a <span style="color: #f50">theat produed</span> may vary from <span style="color: #cc0">very slight interruptions</span> to <span style="color: #f50">imposed fines</span> due to <span style="color: #f50">lack of due diligence</span>, <span style="color: #c00">organization leadership rupture</span> as the result of <span style="color: #c00">incarceration, bankruptcy or total cease of the organization</span>.

    The <span style="color: #cc0">graveness</span> of <span style="color: #f50">interruptions</span> to the <span style="color: #cc0">organization's capability to <span style="color: #0c0">achieve its <span style="color: #af0">goal</span></span></span> is denominated <span style="color: #cc0">impact</span>.

1. Exposure factor

    <span style="color: #cc0">Exposure factor</span> is defined as the <span style="color: #cc0"><span style="color: #f50">opportunity</span> that a <span style="color: #c00">threat</span> may cause a <span style="color: #c00">loss</span></span>.

    The <span style="color: #cc0">exposure factor</span> plays a key role in <span style="color: #cc0">risk calculation</span>. Though the <span style="color: #cc0">likelihood of an attack may be <span style="color: #c00">high</span> and its impact <span style="color: #c00">grave</span></span>, if the software is <span style="color: #55f">designed, developed and implemented</span> taking <span style="color: #f50">security</span> into account, the <span style="color: #cc0"><span style="color: #f50">exposure factor</span> may be <span style="color: #0c0">low</span>, <span style="color: #0c0">reducing the general risk</span> of vulnerability explotation</span>.

1. Security Controls

    <span style="color: #cc0">Security controls</span> are mechanisms used to <span style="color: #0c0">mitigate</span> <span style="color: #c00">threats</span> to <span style="color: #55f">software and systems</span>. These mechanisms can be of technical, administrative or physical nature. Security controls can be classified broadly into <span style="color: #f50">countermeasures</span> and <span style="color: #f50">safeguards</span>.

    <span style="color: #f50">Countermeasures</span> are <span style="color: #f50">security controls</span> <span style="color: #cc0">applied after the threat has been produced</span>, which implies a <span style="color: #cc0">reactive nature</span>.

    On the other hand, <span style="color: #f50">safeguards</span> are <span style="color: #cc0">proactive security controls</span>.

    Security controls do NOT eliminate the <span style="color: #c00">threat</span> altogether, instead they're <span style="color: #cc0">integrated into the <span style="color: #55f">software</span> or <span style="color: #55f">system</span> to reduce the <span style="color: #f50">likelihood</span> of it <span style="color: #f50">becoming real</span></span>.
    
    <span style="color: #cc0"><span style="color: #f50">Vulnerabilities</span> are reduced through <span style="color: #f50">security controls</span></span>, however, it must be noted that <span style="color: #cc0">the incorrect implementation of <span style="color: #f50">security controls</span> may become a <span style="color: #c00">threat</span> in itself</span>.

1. Total Risk

    This is the <span style="color: #cc0">likelihood of an <span style="color: #f50">unwanted event </span>happenning</span>. This is traditionally <span style="color: #cc0">calculated using factors such as <span style="color: #f50">asset value</span>, <span style="color: #c00">threat</span> and <span style="color: #f50">vulnerability</span></span>. This is <span style="color: #cc0">the general risk of the system, before <span style="color: #f50">security controls</span> are applied</span>. It <span style="color: #cc0">can be expressed <span style="color: #f50">qualitatively</span> (i.e. low, meidium, high) or <span style="color: #f50">quantitatively</span> (i.e. numbers, e.g. percentiles)</span>.

    The <span style="color: #cc0">sum of all risks associated to an <span style="color: #f50">asset</span>, <span style="color: #f50">process</span> or even an <span style="color: #f50">entire business</span></span> is denominated total risk.

1. Residual Risk

    This is the <span style="color: #cc0"><span style="color: #f50">risk</span> remaining after the implementation of <span style="color: #f50">mitigating security controls</span></span> (either countermeasures or safeguards).

1. Risk Management

    This is the <span style="color: #cc0">general process</span> of <span style="color: #cc0">decision making</span> towards identifying <span style="color: #c00">threats</span> and <span style="color: #f50">vulnerabilities</span> and their <span style="color: #f50">potential impact</span>, determine such events' <span style="color: #f50">mitigation costs</span> and <span style="color: #cc0">decide which actions are viable to <span style="color: #f50">control</span> those <span style="color: #f50">risks</span></span>.

1.  Risk Evaluation

    This is <span style="color: #cc0">the process to analyze an environment to identify risks (threats and vulnerabilities) and determine their impact</span>. Also called <span style="color: #cc0">Risk Analysis</span>.

    Qualitative risk evaluation is the process of subjectively determining the impact of an event affecting a project, program or busieness.
    
    Completing a qualitative risk evaluation <span style="color: #cc0">generally implies the use of <span style="color: #f50">experts' judgement</span>, <span style="color: #f50">their experience</span> or <span style="color: #f50">the group's consensus</span> to determine a <span style="color: #f50">level of exposure</span></span> to risk. In qualitative risk management, <span style="color: #cc0">two elements are used to judge</span>: <span style="color: #f50">threat impact</span> and its <span style="color: #f50">likelihood</span>. It's a simple schema, like high, medium or low classifications, to assign an impact level and a likelihood level,
    
    e.g. a threat has a high impact and a high likelihood of ocurring, risk exposure is high and will likely require some action to reduce its threat. On the contrary, if the impact is low with a low chance, risk exposure is low and will likely require no action to reduce this threat. Qualitative risk evaluation is used to prioritize risk management activities, so there's no need for an exact quantification.
    
1. Qualitative Matrix

    A method to express a collection of <span style="color: #f50">qualitative information</span> is in the form of a <span style="color: #f50">matrix</span>.
    
    <span style="color: #cc0">The first step forming a matrix is defining the values of <span style="color: #f50">high</span>, <span style="color: #f50">medium</span> and <span style="color: #f50">low</span></span>, these values are organized in a matrix with rows as specific problems or elements examined. 
    
    Columns represent the security element affected, generally these are<span style="color: #f50">confidentiality</span>, <span style="color: #f50">integrity</span> and <span style="color: #f50">availability</span>.
    
    When a row crosses with a column, the right value is chosen:

|  | Confidentiality | Integrity | Availability |
| - | - | - | - |
| Database | Medium | High | Medium |
| API 1 | Medium | Low | Medium |
| API 2 | Low | Low | Medium |

1. 

</details>

</details>

<span style="color: #"></span>
